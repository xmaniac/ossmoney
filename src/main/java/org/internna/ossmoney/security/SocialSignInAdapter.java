package org.internna.ossmoney.security;

import org.internna.ossmoney.data.UserRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.data.InvestmentPortfolioRepository;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.springframework.social.connect.Connection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.internna.ossmoney.model.security.CombinedUser.getDefaultAuthorities;

@Transactional
public final class SocialSignInAdapter implements SignInAdapter {

    private static final String LOCAL_DEPLOYMENT = "local";

    private @Value("${deployment.environment:local}") String deployment;

    private @Autowired UserRepository userRepository;
    private @Autowired InvestmentPortfolioRepository investmentPortfolioRepository;

    @Transactional
    public @Override String signIn(final String userId, final Connection<?> connection, final NativeWebRequest request) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            final User.UserBuilder builder = new User.UserBuilder(userId).addLocale(request != null ? request.getLocale() : null).administrator(SocialSignInAdapter.LOCAL_DEPLOYMENT.equals(deployment));
            user = userRepository.save(builder.build());
            investmentPortfolioRepository.save(new InvestmentPortfolio(user));
        }
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userId, null, getDefaultAuthorities(user));
        SecurityContextHolder.getContext().setAuthentication(token);
        return userId;
    }

}

package org.internna.ossmoney.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.transaction.ReconcileDTO;
import org.internna.ossmoney.model.transaction.TransactionDTO;

public interface TransactionService {

    Optional<Account> delete(String uuid);

    Optional<Transaction> find(String uuid);

    Account[] reconcile(ReconcileDTO reconciliation);

    Optional<Transaction> save(TransactionDTO transaction);

    Iterable<Transaction> incomeAndExpenses(final Date date);

    List<Transaction> obtainTransactions(String accountId, Date from, Date to);

}

package org.internna.ossmoney.services;

import java.util.List;
import java.util.Optional;
import java.util.Collection;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;
import org.internna.ossmoney.model.investment.pm.PreciousMetalProduct;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransactionDTO;

public interface InvestmentService {

    Optional<Deposit> save(DepositDTO deposit);

    Optional<Deposit> findDeposit(String uuid);

    Optional<Deposit> cancel(DepositDTO deposit);

    Optional<InvestmentPortfolio> retrievePortfolio();

    Collection<PreciousMetalProduct> obtainPreciousMetalProducts();

    List<Optional<PreciousMetalPrice>> retrievePreciousMetalPrices();

    Optional<PreciousMetalProduct> save(PreciousMetalProduct product);

    Optional<PreciousMetal> buyPreciousMetal(PreciousMetalTransactionDTO transaction);

    Optional<PreciousMetal> sellPreciousMetal(PreciousMetalTransactionDTO transaction);

}

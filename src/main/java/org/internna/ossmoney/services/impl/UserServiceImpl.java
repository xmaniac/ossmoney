package org.internna.ossmoney.services.impl;

import java.util.Locale;
import java.util.Optional;

import javax.money.MonetaryCurrencies;

import org.internna.ossmoney.data.UserRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.security.UserDTO;
import org.springframework.stereotype.Service;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Service
@Transactional
public final class UserServiceImpl implements UserService {

    private @Autowired UserRepository userRepository;

    public @Override Optional<User> update(final UserDTO user) {
        final Optional<User> loaded = currentUser();
        return (loaded.isPresent()) && (user != null) ? Optional.of(updateUser(loaded.get(), user)) : Optional.empty();
    }

    @Transactional(readOnly = true)
    public @Override Optional<User> currentUser() {
        User user = null;
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            user = userRepository.findOne(authentication.getName());
        }
        return Optional.of(user);
    }

    private User updateUser(final User user, final UserDTO dto) {
        User merged = user;
        boolean updated = false;
        final String locale = dto.getLocale();
        if (hasText(locale) && !locale.equals(user.getLocale().getDisplayLanguage())) {
            updated = true;
            user.setLocale(new Locale.Builder().setLanguage(locale.substring(0, 2)).setRegion(locale.substring(3, 5)).build());
        }
        if (hasText(dto.getSkin()) && !dto.getSkin().equalsIgnoreCase(user.getSkin())) {
            updated = true;
            user.setSkin(dto.getSkin().toLowerCase());
        }
        if (hasText(dto.getCurrency()) && !dto.getCurrency().equalsIgnoreCase(user.getCurrency().getCurrencyCode())) {
            updated = true;
            user.setCurrency(MonetaryCurrencies.getCurrency(dto.getCurrency()));
        }
        if (updated) {
            merged = userRepository.save(user);
        }
        return merged;
    }
}

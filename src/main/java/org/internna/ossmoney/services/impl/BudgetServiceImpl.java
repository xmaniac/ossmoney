package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.Optional;
import java.util.Collection;
import java.util.Collections;
import org.javamoney.moneta.Money;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.budget.Budget;
import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.data.BudgetRepository;
import org.internna.ossmoney.model.budget.BudgetDTO;
import org.internna.ossmoney.services.BudgetService;
import org.internna.ossmoney.model.account.AccountType;
import org.internna.ossmoney.data.TransactionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Service
@Transactional
public final class BudgetServiceImpl implements BudgetService {

    private static final String INVALID_BUDGET = "Invalid budget requested";

    private @Autowired UserService userService;
    private @Autowired BudgetRepository budgetRepository;
    private @Autowired TransactionRepository transactionRepository;

    @Transactional(readOnly = true)
    public @Override Optional<Budget> find(final String uuid) {
        return hasText(uuid) ? findById(uuid) : Optional.empty();
    }

    private Optional<Budget> findById(final String uuid) {
        return findById(uuid, userService.currentUser());
    }

    private Optional<Budget> findById(final String uuid, final Optional<User> user) {
        return user.isPresent() ? findById(uuid, user.get()) : Optional.empty();
    }

    private Optional<Budget> findById(final String uuid, final User user) {
        final Budget budget = budgetRepository.findOne(uuid);
        if (budget != null) {
            if (!budget.getOwner().equals(user)) {
                throw new IllegalArgumentException(INVALID_BUDGET);
            }
        }
        return Optional.ofNullable(budget);
    }

    @Transactional
    public @Override Optional<Budget> save(final BudgetDTO budget) {
        Optional<Budget> saved = null;
        if (budget != null) {
            final Optional<User> user = userService.currentUser();
            if (user.isPresent()) {
                saved = hasText(budget.getUuid()) ? merge(budget, user.get()) : create(budget, user.get());
                saved = saved.isPresent() ? Optional.of(findBudgetTransactions(budgetRepository.save(saved.get()))) : null;
            }
        }
        return saved != null ? saved : Optional.empty();
    }

    private Optional<Budget> create(final BudgetDTO dto, final User user) {
        return Optional.of(dto.asBudget(user));
    }

    private Optional<Budget> merge(final BudgetDTO dto, final User user) {
        Optional<Budget> loaded = findById(dto.getUuid());
        if (loaded.isPresent()) {
            final Budget budget = loaded.get();
            budget.setTo(dto.getTo());
            budget.setName(dto.getName());
            budget.setFrom(dto.getFrom());
            budget.setSubcategory(dto.getSubcategory());
            budget.setAmount(Money.of(budget.getAmount().getCurrency(), dto.getAmount()));
            loaded = Optional.of(budget);
        }
        return loaded;
    }

    @Transactional(readOnly = true)
    public @Override Collection<Budget> retrieveBudget() {
        Collection<Budget> budgets = null;
        final Optional<User> user = userService.currentUser();
        if (user.isPresent()) {
            budgets = budgetRepository.findBudgets(user.get(), new Date());
            if (CollectionUtils.isNotEmpty(budgets)) {
                budgets.forEach(budget -> findBudgetTransactions(budget));
            }
        }
        return CollectionUtils.isEmpty(budgets) ? Collections.emptyList() : Collections.unmodifiableCollection(budgets);
    }

    private Budget findBudgetTransactions(final Budget budget) {
        final Date today = new Date();
        final Date from = DateUtils.add(today, -180);
        budget.setSpent(budgetRepository.sumTransactions(budget.getOwner(), budget.getSubcategory().name(), DateUtils.getMonthStartDate(today), DateUtils.getMonthEndDate(today)));
        budget.setTransactions(transactionRepository.findExpensesByDate(from.after(budget.getFrom()) ? from : budget.getFrom(), budget.getOwner(), AccountType.values(), budget.getSubcategory()));
        return budget;
    }

    @Transactional
    public @Override Boolean remove(final String uuid) {
        final Optional<Budget> budget = find(uuid);
        final Boolean removed = budget.isPresent();
        if (removed) {
            final Budget loaded = budget.get();
            loaded.setVisible(false);
            budgetRepository.save(loaded);
        }
        return removed;
    }

}

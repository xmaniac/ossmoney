package org.internna.ossmoney.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.javamoney.moneta.Money;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.zip.ZipEntry;
import javax.money.MonetaryAmount;
import java.util.zip.ZipOutputStream;
import java.io.ByteArrayOutputStream;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.internna.ossmoney.model.Country;
import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.util.MoneyUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.data.AccountRepository;
import org.internna.ossmoney.data.CountryRepository;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.data.TransactionRepository;
import org.internna.ossmoney.model.account.FinancialInstitution;
import org.internna.ossmoney.data.FinancialInstitutionRepository;

import static org.springframework.util.StreamUtils.copy;
import static org.internna.ossmoney.util.StringUtils.hasText;
import static org.internna.ossmoney.util.StringUtils.stripUTF;
import static org.internna.ossmoney.util.StringUtils.asStream;

@Service
public final class AccountServiceImpl implements AccountService {

    private static final String INVALID_ACCOUNT = "Invalid account requested";
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private @Autowired UserService userService;
    private @Autowired CountryRepository countryRepository;
    private @Autowired AccountRepository accountRepository;
    private @Autowired TransactionRepository transactionRepository;
    private @Autowired FinancialInstitutionRepository financialInstitutionRepository;

    @Transactional(readOnly = true)
    public @Override Optional<Account> find(final String uuid) {
        final Account account = hasText(uuid) ? visible(accountRepository.findOne(uuid)) : null;
        if (account != null) {
            final Optional<User> user = userService.currentUser();
            if (!user.isPresent() || !account.getOwner().equals(user.get())) {
                throw new IllegalArgumentException(AccountServiceImpl.INVALID_ACCOUNT);
            }
        }
        return Optional.ofNullable(account);
    }

    private Account visible(final Account account) {
        return (account != null) && account.isVisible() ? account : null;
    }

    @Transactional(readOnly = true)
    public @Override List<Account> findAccounts() {
        final Optional<User> user = userService.currentUser();
        final List<Account> accounts = user.isPresent() ? accountRepository.findByOwnerAndVisible(user.get(), Boolean.TRUE) : null;
        return CollectionUtils.isEmpty(accounts) ? Collections.emptyList() : Collections.unmodifiableList(accounts);
    }

    @Transactional(readOnly = true)
    public @Override List<FinancialInstitution> findFinancialInstitutions(final String country) {
        Country found = countryRepository.findOne(country);
        if (found == null) {
            found = countryRepository.findByCountryKey(country);
        }
        final List<FinancialInstitution> banks = financialInstitutionRepository.findByCountry(found);
        return banks != null ? banks : Collections.emptyList();
    }

    @Transactional
    public @Override Optional<Account> save(final Account account, final BigDecimal balance) {
        Account saved = null;
        if (account != null) {
            final Optional<User> user = userService.currentUser();
            if (user.isPresent()) {
                saved = hasText(account.getUuid()) ? merge(account, balance, user.get()) : create(account, balance, user.get());
                saved = accountRepository.save(saved);
            }
        }
        return Optional.ofNullable(saved);
    }

    private Account create(final Account account, final BigDecimal balance, final User user) {
        account.setOwner(user);
        account.setFinancialInstitution(financialInstitutionRepository.findOne(account.getFinancialInstitution().getUuid()));
        account.setInitialBalance(Money.of(account.getFinancialInstitution().getCountry().getCurrency(), balance));
        return account;
    }

    private Account merge(final Account account, final BigDecimal balance, final User user) {
        Account merged = null;
        final Optional<Account> loaded = find(account.getUuid());
        if (loaded.isPresent()) {
            merged = loaded.get();
            merged.setIban(account.getIban());
            merged.setName(account.getName());
            merged.setOpened(account.getOpened());
            merged.setFavorite(account.getFavorite());
            merged.setAccountType(account.getAccountType());
            merged.setDescription(account.getDescription());
            if ((account.getFinancialInstitution() != null) && (hasText(account.getFinancialInstitution().getUuid()))) {
                merged.setFinancialInstitution(financialInstitutionRepository.getOne(account.getFinancialInstitution().getUuid()));
            }
            if (!MoneyUtils.equals(merged.getInitialBalance(), balance)) {
                merged.setInitialBalance(Money.of(merged.getInitialBalance().getCurrency(), balance));
                recalculateCurrentBalance(merged);
            }
        }
        return merged;
    }

    private void recalculateCurrentBalance(final Account account) {
        final BigDecimal sum = transactionRepository.sumTransactions(account.getUuid());
        if ((sum != null) || BigDecimal.ZERO.equals(sum)) {
            final MonetaryAmount balance = account.getInitialBalance();
            account.setCurrentBalance(balance.add(Money.of(balance.getCurrency(), sum)));
        } else {
            account.setCurrentBalance(account.getInitialBalance());
        }
    }

    @Transactional(readOnly = true)
    public @Override byte[] export() throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final ZipOutputStream zos = new ZipOutputStream(bos);
        findAccounts().forEach(account -> {
            try {
                zos.putNextEntry(new ZipEntry(stripUTF(account.getName()) + ".qif"));
                copy(asStream(qif(account)), zos);
                zos.closeEntry();
            } catch (final IOException ioe) {
                logger.warn("Could not add ZipEntry [{}]: {}", account.getName(), ioe.getMessage());
            }
        });
        zos.close();
        return bos.toByteArray();
    }

    private String qif(final Account account) {
        final StringBuilder qif = new StringBuilder(account.asQIF());
        account.getTransactions().forEach(transaction -> {
            if (transaction.isVisible() & !transaction.isReconciled()) {
                qif.append(transaction.asQIF());
            }
        });
        return qif.toString();
    }

    @Transactional
    public @Override Optional<Account> close(final String uuid) {
        Optional<Account> loaded = find(uuid);
        if (loaded.isPresent()) {
            Account account = loaded.get();
            if (account.getClosed() == null) {
                account.setClosed(new Date());
            } else {
                account.setVisible(false);
            }
            account = accountRepository.save(account);
            loaded = Optional.ofNullable(account.isVisible() ? account : null);
        }
        return loaded;
    }

    @Transactional
    public @Override Optional<Account> reopen(final String uuid) {
        Optional<Account> account = find(uuid);
        if (account.isPresent()) {
            final Account closed = account.get();
            if (closed.getClosed() != null) {
                closed.setClosed(null);
                account = Optional.ofNullable(accountRepository.save(closed));
            }
        }
        return account;
    }

    @Transactional
    public @Override Optional<FinancialInstitution> save(final FinancialInstitution bank) {
        FinancialInstitution institution = null;
        if ((bank != null) && (hasText(bank.getName()))) {
            institution = new FinancialInstitution();
            institution.setWeb(bank.getWeb());
            institution.setName(bank.getName());
            institution.setCountry(countryRepository.findByCountryKey(bank.getCountry().getCountryKey()));
            institution = financialInstitutionRepository.save(institution);
        }
        return Optional.ofNullable(institution);
    }

}

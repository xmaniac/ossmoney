package org.internna.ossmoney.config;

import org.javamoney.moneta.Money;
import java.math.BigDecimal;
import javax.money.MonetaryContext;
import javax.money.MonetaryAmountFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

@Component
public final class HibernateAndMonetaObjectMapper extends ObjectMapper {

    private static final long serialVersionUID = 2517924899645751510L;

    public HibernateAndMonetaObjectMapper() throws ClassNotFoundException, LinkageError {
        configureExternalObjects();
        registerModule(new Jdk8Module());
        registerModule(hibernateModule());
    }

    private Hibernate4Module hibernateModule() {
        final Hibernate4Module module = new Hibernate4Module();
        module.disable(Hibernate4Module.Feature.USE_TRANSIENT_ANNOTATION);
        return module;
    }

    private void configureExternalObjects() throws ClassNotFoundException, LinkageError {
        addMixIn(Money.class, MixIn.class);
        addMixIn(Class.forName("org.javamoney.moneta.internal.JDKCurrencyAdapter"), CurrencyMixIn.class);
    }

    @JsonIgnoreType
    abstract class MixIn {
        abstract @JsonIgnore boolean isZero();

        abstract @JsonIgnore boolean isPositive();

        abstract @JsonIgnore boolean isNegative();

        abstract @JsonIgnore boolean isNegativeOrZero();

        abstract @JsonIgnore boolean isPositiveOrZero();

        abstract @JsonIgnore BigDecimal getNumberStripped();

        abstract @JsonIgnore MonetaryContext getMonetaryContext();

        abstract @JsonIgnore MonetaryAmountFactory<Money> getFactory();
    }

    @JsonIgnoreType
    abstract class CurrencyMixIn {
        abstract @JsonIgnore int getNumericCode();

        abstract @JsonIgnore int getDefaultFractionDigits();
    }

}

package org.internna.ossmoney.aop;

import org.hibernate.Session;
import javax.persistence.EntityManager;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.internna.ossmoney.model.AbstractEntity;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
@Component
public class HibernateFilterProxy {

    private @Autowired EntityManager entityManager;

    @Before("execution(* org.internna.ossmoney.data.*.*(..))")
    private void enableFilters() {
        final Session hibernateSession = entityManager.unwrap(Session.class);
        if (hibernateSession.getEnabledFilter(AbstractEntity.VISIBLE_ENTITY) == null) {
            hibernateSession.enableFilter(AbstractEntity.VISIBLE_ENTITY).setParameter(AbstractEntity.VISIBLE_PARAM, true);
        }
    }

}

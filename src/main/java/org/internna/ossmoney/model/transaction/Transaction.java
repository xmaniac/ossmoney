package org.internna.ossmoney.model.transaction;

import java.util.Collection;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.internna.ossmoney.util.CollectionUtils;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.util.MoneyUtils;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.AbstractTransaction;

import static org.springframework.util.StringUtils.hasText;

@Entity
@Table(name = Transaction.TRANSACTION)
public class Transaction extends AbstractTransaction implements Comparable<Transaction> {

    public static final String TRANSACTION = "TRANSACTION";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "TRANSACTION_TYPE", nullable = false)
    private TransactionType transactionType;

    @Column(name = "MEMO")
    private String memo;

    @Column(name = "REFERENCE")
    private String referenceNumber;

    @NotNull
    @JsonIgnore
    @Column(name = "RECONCILED", nullable = false)
    private boolean reconciled;

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "ACCOUNT", nullable = false)
    private Account account;

    @Column(name = "TAG")
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Tags.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "TAGS", joinColumns = @JoinColumn(name = "UUID"))
    private Collection<Tags> tags;

    public final int compareTo(final Transaction other) {
        return other == null ? 1 : getOperationDate() == null ? other.getOperationDate() == null ? 0 : -1 : other.getOperationDate() == null ? 1 : getOperationDate().compareTo(other.getOperationDate());
    }

    public final String asQIF() {
        final StringBuilder qif = new StringBuilder("!Type:Cash\n").append(DateUtils.asQIF(getOperationDate())).append("\n");
        if (hasText(memo)) {
            qif.append("M").append(memo).append("\n");
        }
        qif.append("T").append(MoneyUtils.format(getAmount()));
        if (CollectionUtils.isNotEmpty(tags)) {
            qif.append("\nL").append(tags);
        }
        return qif.append("\n^\n").toString();
    }

    public final TransactionType getTransactionType() {
        return transactionType;
    }

    public final void setTransactionType(final TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public final String getMemo() {
        return memo;
    }

    public final void setMemo(final String memo) {
        this.memo = memo;
    }

    public final Account getAccount() {
        return account;
    }

    public final boolean isReconciled() {
        return reconciled;
    }

    public final void setReconciled(final boolean reconciled) {
        this.reconciled = reconciled;
    }

    public final void setAccount(final Account account) {
        this.account = account;
    }

    public final String getReferenceNumber() {
        return referenceNumber;
    }

    public final void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public final Collection<Tags> getTags() {
        return tags;
    }

    public final void setTags(final Collection<Tags> tags) {
        this.tags = tags;
    }

}

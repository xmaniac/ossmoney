package org.internna.ossmoney.model.account;

import org.internna.ossmoney.model.Country;

public final class FinancialInstitutionDTO {

    private String name;
    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(final Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public FinancialInstitution asFinancialInstitution() {
        final FinancialInstitution financialInstitution = new FinancialInstitution();
        financialInstitution.setName(getName());
        financialInstitution.setCountry(getCountry());
        return financialInstitution;
    }

}

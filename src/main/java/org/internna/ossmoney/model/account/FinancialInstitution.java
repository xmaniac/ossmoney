package org.internna.ossmoney.model.account;

import java.util.Set;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import org.internna.ossmoney.model.Country;
import org.internna.ossmoney.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = FinancialInstitution.FINANCIAL_INSTITUTION)
public class FinancialInstitution extends AbstractEntity {

    public static final String FINANCIAL_INSTITUTION = "FINANCIAL_INSTITUTION";

    public FinancialInstitution() {
        // Default JSON/JPA constructor
    }

    public FinancialInstitution(final String uuid) {
        setUuid(uuid);
    }

    @NotNull
    @Size(min = 3)
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "WEB")
    private String web;

    @Column(name = "ICON")
    private String icon;

    @Column(name = "PROVIDER")
    private String providerClass;

    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COUNTRY", nullable = false)
    private Country country;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "financialInstitution")
    private Set<Account> accounts;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getWeb() {
        return web;
    }

    public final void setWeb(final String web) {
        this.web = web;
    }

    public final String getIcon() {
        return icon;
    }

    public final void setIcon(final String icon) {
        this.icon = icon;
    }

    public final Country getCountry() {
        return country;
    }

    public final void setCountry(final Country country) {
        this.country = country;
    }

    public final Set<Account> getAccounts() {
        return accounts;
    }

    public final void setAccounts(final Set<Account> accounts) {
        this.accounts = accounts;
    }

    public final String getProviderClass() {
        return providerClass;
    }

    public final void setProviderClass(final String providerClass) {
        this.providerClass = providerClass;
    }

}

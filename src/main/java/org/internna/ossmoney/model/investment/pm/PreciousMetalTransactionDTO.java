package org.internna.ossmoney.model.investment.pm;

import java.util.Date;
import java.math.BigDecimal;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;

public final class PreciousMetalTransactionDTO {

    private Date operationDate;
    private PreciousMetalType metalType;
    private Integer quantity = 1, mintedYear;
    private String uuid, product, currency, account;
    private PreciousMetalTransactionType transactionType;
    private BigDecimal amount, weight, weightInMetal, purity;

    public PreciousMetalTransaction asTransaction(final PreciousMetal preciousMetal, final PreciousMetalProduct product) {
        final PreciousMetalTransaction transaction = new PreciousMetalTransaction();
        if (mintedYear != null) {
            transaction.setMintedYear(mintedYear);
        }
        transaction.setProduct(product);
        transaction.setQuantity(quantity);
        transaction.setAmount(getMoney());
        transaction.setWeight(weight.floatValue());
        transaction.setPreciousMetal(preciousMetal);
        transaction.setOperationDate(operationDate);
        transaction.setTransactionType(transactionType);
        transaction.setWeightInMetal(weightInMetal.floatValue());
        return transaction;
    }

    public MonetaryAmount getMoney() {
        return Money.of(currency, amount);
    }

    public Integer getMintedYear() {
        return mintedYear;
    }

    public void setMintedYear(final Integer mintedYear) {
        this.mintedYear = mintedYear;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(final Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(final String product) {
        this.product = product;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public PreciousMetalType getMetalType() {
        return metalType;
    }

    public void setMetalType(final PreciousMetalType metalType) {
        this.metalType = metalType;
    }

    public PreciousMetalTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(final PreciousMetalTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(final BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getWeightInMetal() {
        return weightInMetal;
    }

    public void setWeightInMetal(final BigDecimal weightInMetal) {
        this.weightInMetal = weightInMetal;
    }

    public BigDecimal getPurity() {
        return purity;
    }

    public void setPurity(final BigDecimal purity) {
        this.purity = purity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

}

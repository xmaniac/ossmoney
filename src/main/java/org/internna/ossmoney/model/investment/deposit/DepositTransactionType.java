package org.internna.ossmoney.model.investment.deposit;

public enum DepositTransactionType {

	CONTRACT, WITHDRAWAL, PARTIAL_CANCELLATION;

}

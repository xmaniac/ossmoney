package org.internna.ossmoney.model.investment.pm;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.model.AbstractTransaction;

@Entity
@Table(name = PreciousMetalTransaction.PRECIOUS_METAL_TRANSACTION)
public class PreciousMetalTransaction extends AbstractTransaction {

    public static final String PRECIOUS_METAL_TRANSACTION = "PRECIOUS_METAL_TRANSACTION";

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "PRECIOUS_METAL", nullable = false, updatable = false)
    private PreciousMetal preciousMetal;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "TRANSACTION_TYPE", nullable = false)
    private PreciousMetalTransactionType transactionType;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "PRODUCT", nullable = false, updatable = false)
    private PreciousMetalProduct product;

    @Column(name = "MINTED_YEAR")
    private int mintedYear;

    @NotNull
    @Column(name = "QUANTITY", nullable = false)
    private int quantity;

    @NotNull
    @Column(name = "REMAINING", nullable = false)
    private int remaining;

    @NotNull
    @Column(name = "WEIGHT", nullable = false, columnDefinition = "DECIMAL")
    private float weight;

    @NotNull
    @Column(name = "WEIGHT_METAL", nullable = false, columnDefinition = "DECIMAL")
    private float weightInMetal;

    @NotNull
    @Column(name = "PURITY", nullable = false, columnDefinition = "DECIMAL")
    private float purity;

    @PrePersist
    protected final void calculateWeightAndPurity() {
        if (weightInMetal <= 0) {
            setWeightInMetal(getWeight());
        }
        if (purity <= 0) {
            setPurity(0.9999f);
        }
        setRemaining(quantity);
    }

    public final PreciousMetal getPreciousMetal() {
        return preciousMetal;
    }

    public final void setPreciousMetal(final PreciousMetal preciousMetal) {
        this.preciousMetal = preciousMetal;
    }

    public final PreciousMetalTransactionType getTransactionType() {
        return transactionType;
    }

    public final void setTransactionType(final PreciousMetalTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public final PreciousMetalProduct getProduct() {
        return product;
    }

    public final void setProduct(final PreciousMetalProduct product) {
        this.product = product;
    }

    public final float getWeight() {
        return weight;
    }

    public final void setWeight(final float weight) {
        this.weight = weight;
    }

    public final float getWeightInMetal() {
        return weightInMetal;
    }

    public final void setWeightInMetal(final float weightInMetal) {
        this.weightInMetal = weightInMetal;
    }

    public final float getPurity() {
        return purity;
    }

    public final void setPurity(final float purity) {
        this.purity = purity;
    }

    public int getMintedYear() {
        return mintedYear;
    }

    public void setMintedYear(final int mintedYear) {
        this.mintedYear = mintedYear;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(final int remaining) {
        this.remaining = remaining;
    }

}

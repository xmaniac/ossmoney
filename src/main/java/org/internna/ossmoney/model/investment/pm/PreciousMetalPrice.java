package org.internna.ossmoney.model.investment.pm;

import java.util.Date;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.money.MonetaryAmount;
import javax.persistence.Temporal;
import javax.persistence.Enumerated;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Columns;
import org.internna.ossmoney.model.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = PreciousMetalPrice.PRECIOUS_METAL_PRICE)
public class PreciousMetalPrice extends AbstractEntity {

    static final String MONEY_TYPE = "MONEY_TYPE";
    public static final String PRECIOUS_METAL_PRICE = "PRECIOUS_METAL_PRICE";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "METAL", nullable = false)
    private PreciousMetalType preciousMetalType;

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    @Column(name = "OPERATION_DATE", nullable = false)
    private Date date;

    @NotNull
    @Type(type = PreciousMetalPrice.MONEY_TYPE)
    @Columns(columns = {@Column(name = "SPOT_CURRENCY_USD", nullable = false), @Column(name = "SPOT_AMOUNT_USD", nullable = false)})
    private MonetaryAmount spot_usd;

    @NotNull
    @Type(type = PreciousMetalPrice.MONEY_TYPE)
    @Columns(columns = {@Column(name = "SPOT_CURRENCY_EUR", nullable = false), @Column(name = "SPOT_AMOUNT_EUR", nullable = false)})
    private MonetaryAmount spot_eur;

    @NotNull
    @Type(type = PreciousMetalPrice.MONEY_TYPE)
    @Columns(columns = {@Column(name = "SPOT_CURRENCY_GBP", nullable = false), @Column(name = "SPOT_AMOUNT_GBP", nullable = false)})
    private MonetaryAmount spot_gbp;

    public final PreciousMetalType getPreciousMetalType() {
        return preciousMetalType;
    }

    public final void setPreciousMetalType(final PreciousMetalType preciousMetalType) {
        this.preciousMetalType = preciousMetalType;
    }

    public final Date getDate() {
        return date;
    }

    public final void setDate(final Date date) {
        this.date = date;
    }

    public MonetaryAmount getSpot_usd() {
        return spot_usd;
    }

    public void setSpot_usd(final MonetaryAmount spot_usd) {
        this.spot_usd = spot_usd;
    }

    public MonetaryAmount getSpot_eur() {
        return spot_eur;
    }

    public void setSpot_eur(final MonetaryAmount spot_eur) {
        this.spot_eur = spot_eur;
    }

    public MonetaryAmount getSpot_gbp() {
        return spot_gbp;
    }

    public void setSpot_gbp(final MonetaryAmount spot_gbp) {
        this.spot_gbp = spot_gbp;
    }

}

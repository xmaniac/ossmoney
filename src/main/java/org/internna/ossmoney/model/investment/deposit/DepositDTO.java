package org.internna.ossmoney.model.investment.deposit;

import java.util.Date;
import java.math.BigDecimal;

public final class DepositDTO {

    private Date opened;
    private int duration;
    private float annualPercentageYield;
    private BigDecimal balance, interest;
    private String uuid, name, financialInstitution, account, interestAccount;

    public float getAnnualPercentageYield() {
        return annualPercentageYield;
    }

    public void setAnnualPercentageYield(final float annualPercentageYield) {
        this.annualPercentageYield = annualPercentageYield;
    }

    public Date getOpened() {
        return opened;
    }

    public void setOpened(Date opened) {
        this.opened = opened;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(final BigDecimal balance) {
        this.balance = balance;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getFinancialInstitution() {
        return financialInstitution;
    }

    public void setFinancialInstitution(final String financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(final BigDecimal interest) {
        this.interest = interest;
    }

    public String getInterestAccount() {
        return interestAccount;
    }

    public void setInterestAccount(final String interestAccount) {
        this.interestAccount = interestAccount;
    }

}

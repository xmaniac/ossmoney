package org.internna.ossmoney.model.budget;

import java.util.Date;
import java.math.BigDecimal;
import org.javamoney.moneta.Money;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.transaction.Tags;

public final class BudgetDTO {

    private Date from, to;
    private Tags subcategory;
    private BigDecimal amount;
    private String name, uuid;

    public Budget asBudget(final User user) {
        final Budget budget = new Budget();
        budget.setUuid(uuid);
        budget.setName(name);
        budget.setOwner(user);
        budget.setSubcategory(subcategory);
        budget.setAmount(Money.of(user.getCurrency(), this.amount));
        budget.setTo(DateUtils.getMonthEndDate(to != null ? to : new Date()));
        budget.setFrom(DateUtils.getMonthStartDate(from != null ? from : new Date()));
        return budget;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(final Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(final Date to) {
        this.to = to;
    }

    public Tags getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(final Tags subcategory) {
        this.subcategory = subcategory;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

}
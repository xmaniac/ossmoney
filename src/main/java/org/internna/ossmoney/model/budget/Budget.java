package org.internna.ossmoney.model.budget;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Columns;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.EnumType;
import javax.money.MonetaryAmount;
import javax.persistence.Transient;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.util.DateUtils;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.AbstractEntity;
import org.internna.ossmoney.model.transaction.Tags;
import org.internna.ossmoney.model.transaction.Transaction;
import org.springframework.format.annotation.DateTimeFormat;
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency;

@Entity
@Table(name = Budget.BUDGET)
@TypeDef(name = Budget.MONEY_TYPE, typeClass = PersistentMoneyAmountAndCurrency.class)
public class Budget extends AbstractEntity {

    public static final String BUDGET = "BUDGET";
    static final String MONEY_TYPE = "MONEY_TYPE";

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Type(type = Budget.MONEY_TYPE)
    @Columns(columns = {@Column(name = "CURRENCY", nullable = false), @Column(name = "AMOUNT", nullable = false)})
    private MonetaryAmount amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "CATEGORY", nullable = false)
    private Tags subcategory;

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    @Column(name = "DATE_FROM", nullable = false)
    private Date from;

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    @Column(name = "DATE_TO", nullable = false)
    private Date to;

    @NotNull
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "OWNER", nullable = false, updatable = false)
    private User owner;

    @Transient
    private List<Transaction> transactions;

    @Transient
    private BigDecimal spent;

    @PrePersist
    protected final void fillDates() {
        if (to == null) {
            to = DateUtils.add(new Date(), 3650);
        }
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final MonetaryAmount getAmount() {
        return amount;
    }

    public final void setAmount(final MonetaryAmount amount) {
        this.amount = amount;
    }

    public final Tags getSubcategory() {
        return subcategory;
    }

    public final void setSubcategory(final Tags subcategory) {
        this.subcategory = subcategory;
    }

    public final Date getFrom() {
        return from;
    }

    public final void setFrom(final Date from) {
        this.from = from;
    }

    public final Date getTo() {
        return to;
    }

    public final void setTo(final Date to) {
        this.to = to;
    }

    public final User getOwner() {
        return owner;
    }

    public final void setOwner(final User owner) {
        this.owner = owner;
    }

    public final List<Transaction> getTransactions() {
        return transactions;
    }

    public final void setTransactions(final List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public BigDecimal getSpent() {
        return spent == null ? BigDecimal.ZERO : spent.abs();
    }

    public void setSpent(final BigDecimal spent) {
        this.spent = spent;
    }

}

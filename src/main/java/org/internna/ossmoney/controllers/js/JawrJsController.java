package org.internna.ossmoney.controllers.js;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import static net.jawr.web.JawrConstant.JS_TYPE;

@Controller
public final class JawrJsController extends AbstractJawrController {

	public JawrJsController() {
		super(JS_TYPE);
	}

	@RequestMapping(value = {"/jawr*.js", "/gzip*.js", "/gzip*/**/*.js"}, method = RequestMethod.GET)
	@Override public ModelAndView handleRequest(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		return super.handleRequest(request, response);
	}

}

package org.internna.ossmoney.data;

import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;
import org.internna.ossmoney.model.investment.pm.PreciousMetalPrice;

public interface PreciousMetalPriceRepository extends JpaRepository<PreciousMetalPrice, String> {

    PreciousMetalPrice findByPreciousMetalTypeAndDate(PreciousMetalType preciousMetalType, Date date);

    PreciousMetalPrice findTopByPreciousMetalTypeOrderByDateDesc(PreciousMetalType preciousMetalType);

}

package org.internna.ossmoney.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.investment.pm.PreciousMetal;
import org.internna.ossmoney.model.investment.pm.PreciousMetalType;

public interface PreciousMetalRepository extends JpaRepository<PreciousMetal, String> {

    @Query(value = "SELECT p FROM PreciousMetal p WHERE p.visible = TRUE AND p.portfolio.owner = ?1 AND p.preciousMetalType = ?2")
    PreciousMetal findPreciousMetal(User owner, PreciousMetalType metal);

}

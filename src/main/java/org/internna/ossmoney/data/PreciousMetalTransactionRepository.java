package org.internna.ossmoney.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.investment.pm.PreciousMetalTransaction;

public interface PreciousMetalTransactionRepository extends JpaRepository<PreciousMetalTransaction, String> {

}

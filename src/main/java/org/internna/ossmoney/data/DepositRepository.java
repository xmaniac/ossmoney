package org.internna.ossmoney.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;

public interface DepositRepository extends JpaRepository<Deposit, String> {

    @Query("SELECT d FROM Deposit d WHERE d.visible = TRUE AND d.uuid = ?1 AND d.portfolio = ?2")
    Deposit findByPortfolio(String uuid, InvestmentPortfolio portfolio);

}
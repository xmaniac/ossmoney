package org.internna.ossmoney.data;

import java.util.Date;
import java.util.Collection;
import java.math.BigDecimal;
import org.internna.ossmoney.model.budget.Budget;
import org.internna.ossmoney.model.security.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BudgetRepository extends JpaRepository<Budget, String> {

    @Query(value = "SELECT b FROM Budget b WHERE b.visible = TRUE AND b.owner = ?1 AND b.from <= ?2 AND b.to >= ?2")
    Collection<Budget> findBudgets(User user, Date today);

    @Query(value = "SELECT SUM(t.AMOUNT) FROM TRANSACTION t INNER JOIN ACCOUNT a ON t.ACCOUNT = a.UUID INNER JOIN TAGS c ON t.UUID = c.UUID WHERE t.VISIBLE = TRUE AND t.TRANSACTION_TYPE = 'EXPENSE' AND a.OWNER = ?1 AND c.TAG = ?2 AND t.OPERATION_DATE >= ?3 AND t.OPERATION_DATE <= ?4", nativeQuery = true)
    BigDecimal sumTransactions(User user, String tag, Date from, Date to);

}

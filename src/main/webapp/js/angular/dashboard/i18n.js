(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"dashboard.networth": "Patrimonio neto",
			"dashboard.networth.accounts": "Patrimonio neto en cuentas y tarjetas",
			"dashboard.no_accounts": "Cliente sin cuentas",
			"dashboard.ivse": "Ingresos vs Gastos",
			"dashboard.income": "Ingresos",
			"dashboard.expenses": "Gastos",
			"dashboard.expenses.time": "Evolución de los gastos",
			"dashboard.expenses.category": "Gastos por categoría",
			"dashboard.date.0": "Mes en curso",
			"dashboard.date.1": "Últimos 30 días",
			"dashboard.date.3": "Último trimestre",
			"dashboard.date.6": "Último semestre",
			"dashboard.total.income": "Total ingresos",
			"dashboard.total.expenses": "Total gastos",
			"dashboard.total.credit": "Crédito dispuesto"
		});
		$translateProvider.translations('en', {
			"dashboard.networth": "Net worth",
			"dashboard.networth.accounts": "Net worth in accounts & credit cards",
			"dashboard.no_accounts": "No accounts",
			"dashboard.ivse": "Income vs Expenses",
			"dashboard.income": "Income",
			"dashboard.expenses": "Expenses",
			"dashboard.expenses.time": "Expenses timeline",
			"dashboard.expenses.category": "Expenses by category",
			"dashboard.date.0": "Current month",
			"dashboard.date.1": "Last 30 days",
			"dashboard.date.3": "Last quarter",
			"dashboard.date.6": "Last six months",
			"dashboard.total.income": "Total income",
			"dashboard.total.expenses": "Total expense",
			"dashboard.total.credit": "Pending credit"
		});
	}]);

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"AUTOMOBILE": "Automóvil",
			"CAR_BUY": "Compra del coche",
			"CAR_INSURANCE": "Seguro de coche",
			"CAR_RENT": "Alquiler de coche",
			"GASOLINE": "Gasolina",
			"CAR_LOAN_PAYMENT": "Pago letra coche",
			"CAR_MAINTENANCE": "Mantenimiento coche",

			"CHILDCARE": "Familia e hijos",
			"TOYS": "Juguetes",
			"BABYSITTING": "Cuidado de los niños",
			"CHILD_SUPPORT": "Manutención de los niños",

			"EDUCATION": "Educación",
			"TUITION": "Tasas/matrículas",
			"MATERIAL": "Material educativo",
			"STUDENT_LOAN_PAYMENT": "Pago crédito estudios",

			"FINANCIAL": "G. financieros",
			"INTEREST": "Intereses",
			"BANK_CHARGE": "Comisiones bancarias",
			"REIMBURSEMENT": "Reembolso",
			"CREDIT_CARD_FEES": "Comisiones de tarjetas",
			"FINANCE_CHARGE": "Cargos financieros",
			"LOAN": "Préstamos",

			"FOOD": "Comida",
			"COFFEE": "Café y desayuno",
			"GROCERIES": "Alimentación",
			"DINING_OUT": "Restaurantes",
			"VENDING_MACHINE": "Máquinas de vending",

			"HEALTHCARE": "Cuidado de la salud",
			"VISION": "Gafas, lentillas",
			"PHYSICIAN": "Medicina/fisioterapia",
			"DENTAL_CARE": "Dentista",
			"HOSPITAL": "Estancia hospitalaria",
			"PRESCRIPTIONS": "Recetas/medicinas",

			"HOUSEHOLD": "Hogar",
			"RENT": "Alquiler",
			"ASSOCIATION_DUES": "Comunidad de vecinos",
			"FURNITURES": "Muebles",
			"HOME_MAINTENANCE": "Bricolaje/mantenimiento",
			"HOME_TAX": "IBI/Impuestos vivienda",

			"INSURANCE": "Seguros",
			"LIFE_INSURANCE": "Seguro de vida",

			"JOB": "Trabajo",
			"PAYCHECK": "Nómina",
			"BONUS": "Bono/Variable",
			"TRAVEL": "Dietas",

			"LEISURE": "Ocio",
			"VIDEOGAMES": "Videojuegos",
			"CINEMA": "Cine, teatro, conciertos",
			"MAGAZINES": "Revistas y periódicos",
			"BEERS": "Salir de copas",
			"MUSIC": "Música",
			"EVENTS": "Acontecimientos deportivos",
			"VOD": "Videoclub/PayTV",

			"MISC": "Gastos varios",
			"GIFTS": "Regalos",
			"CASH_ATM": "Cajeros automáticos",
			"LOTTERY": "Loterías y premios",
			"GAMBLING": "Casino/apuestas",
			"CHARITY": "Caridad/Obra social",

			"PETS": "Mascotas",
			"PET_FOOD": "Comida mascota",
			"VETERINARIAN": "Veterinario",

			"SHOPPING": "Compras",
			"CLOTHING": "Ropa y complementos",
			"ELECTRONICS": "Electrónica e informática",

			"TAXES": "Tasas e impuestos",
			"GOVERMENT_TAX": "Impuestos",

			"UTILITIES": "Suministros",
			"WATER": "Agua",
			"SEWER": "Alcantarillado",
			"ELECTRICITY": "Electricidad",
			"GAS": "Gas natural",
			"INTERNET": "Internet",
			"TELEPHONE": "Teléfono",
			"GARBAGE": "Recogida de basuras",

			"VACATION": "Vacaciones",
			"TOURISM": "Turismo",
			"TRANSPORTATION": "Vuelos y transportes",
			"LODGING": "Hoteles y alojamiento",
			"LUGGAGE": "Equipaje"
		});
		$translateProvider.translations('en', {
			"AUTOMOBILE": "Automobile",
			"CAR_BUY": "Car purchase",
			"CAR_INSURANCE": "Car insurance",
			"CAR_RENT": "Car rent",
			"GASOLINE": "Gasoline",
			"CAR_LOAN_PAYMENT": "Car loan payment",
			"CAR_MAINTENANCE": "Car maintenance",

			"CHILDCARE": "Childcare",
			"TOYS": "Toys",
			"BABYSITTING": "Babysitting",
			"CHILD_SUPPORT": "Child support",

			"EDUCATION": "Education",
			"TUITION": "Tuition",
			"MATERIAL": "School supplies",
			"STUDENT_LOAN_PAYMENT": "Student loan payment",

			"FINANCIAL": "Financial",
			"INTEREST": "Interest",
			"BANK_CHARGE": "Bank charge",
			"REIMBURSEMENT": "Reimbursement",
			"CREDIT_CARD_FEES": "Credit card fees",
			"FINANCE_CHARGE": "Finance charge",
			"LOAN": "Loan",

			"FOOD": "Food",
			"COFFEE": "Coffee",
			"GROCERIES": "Groceries",
			"DINING_OUT": "Dining out",
			"VENDING_MACHINE": "Vending machine",

			"HEALTHCARE": "Healthcare",
			"VISION": "Vision",
			"PHYSICIAN": "Physician",
			"DENTAL_CARE": "Dental care",
			"HOSPITAL": "Hospital",
			"PRESCRIPTIONS": "Prescriptions",

			"HOUSEHOLD": "Household",
			"RENT": "Rent",
			"ASSOCIATION_DUES": "Association dues",
			"FURNITURES": "Furnitures",
			"HOME_MAINTENANCE": "Home maintenance",
			"HOME_TAX": "Home tax",

			"INSURANCE": "Insurance",
			"LIFE_INSURANCE": "Life insurance",

			"JOB": "Job",
			"PAYCHECK": "Paycheck",
			"BONUS": "Bonus",
			"TRAVEL": "Travel expenses",

			"LEISURE": "Leisure",
			"VIDEOGAMES": "Videogames",
			"CINEMA": "Cinema, theater, concerts",
			"MAGAZINES": "Magazines",
			"BEERS": "Beers",
			"MUSIC": "Music",
			"EVENTS": "Sporting events",
			"VOD": "Video on demand",

			"MISC": "Miscellaneous expenses",
			"CASH_ATM": "ATM",
			"GIFTS": "Gifts",
			"LOTTERY": "Lottery",
			"GAMBLING": "Gambling",
			"CHARITY": "Charity",

			"PETS": "Pets",
			"PET_FOOD": "Pet food",
			"VETERINARIAN": "Veterinarian",

			"SHOPPING": "Shopping",
			"CLOTHING": "Clothing",
			"ELECTRONICS": "Electronics",
			
			"TAXES": "Taxes",
			"GOVERMENT_TAX": "Taxes",

			"UTILITIES": "Utilities",
			"WATER": "Water",
			"SEWER": "Sewer",
			"ELECTRICITY": "Electricity",
			"GAS": "Gas",
			"INTERNET": "Internet",
			"TELEPHONE": "Telephone",
			"GARBAGE": "Garbage",

			"VACATION": "Vacation",
			"TOURISM": "Tourism",
			"TRANSPORTATION": "Flights & transportation",
			"LODGING": "Lodging",
			"LUGGAGE": "Luggage"
		});
	}]);

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"month.0": "Enero",
			"month.1": "Febrero",
			"month.2": "Marzo",
			"month.3": "Abril",
			"month.4": "Mayo",
			"month.5": "Junio",
			"month.6": "Julio",
			"month.7": "Agosto",
			"month.8": "Septiembre",
			"month.9": "Octubre",
			"month.10": "Noviembre",
			"month.11": "Diciembre"
		});
		$translateProvider.translations('en', {
			"month.0": "January",
			"month.1": "February",
			"month.2": "March",
			"month.3": "April",
			"month.4": "May",
			"month.5": "June",
			"month.6": "July",
			"month.7": "August",
			"month.8": "September",
			"month.9": "October",
			"month.10": "November",
			"month.11": "December"
		});
	}]);

})(angular);
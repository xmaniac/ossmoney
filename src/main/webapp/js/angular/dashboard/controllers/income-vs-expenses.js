(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardIncomeVsExpensesController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		if ($rootScope.skin === "brown") {
			$scope.colours_ivse = [
				{fillColor: "rgba(217,178,113,0.2)", strokeColor: "rgba(217,178,113,1)", pointColor: "rgba(217,178,113,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(217,178,113,0.8)"},
				{fillColor: "rgba(194,100,72,0.2)", strokeColor: "rgba(194,100,72,1)", pointColor: "rgba(194,100,72,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(194,100,72,0.8)"}
			];
		}
		$scope.recalculateIncomeVsExpenses = function() {
			$scope.labels_ivse = [];
			$scope.data_ivse = [[], []];
			$scope.series_ivse = [$translate.instant("dashboard.income"), $translate.instant("dashboard.expenses")];
			angular.forEach($rootScope.timeline, function(data) {
				$scope.labels_ivse.push(data["month"]);
				$scope.data_ivse[0].push(data["INCOME"]);
				$scope.data_ivse[1].push(data["EXPENSE"]);
			});
		};
		$rootScope.$on("income-vs-expenses-received", function() {
			$scope.recalculateIncomeVsExpenses();
		});
	}]);

})(angular);
(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardNetWorthController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculateNetWorth = function() {
			$scope.data = [];
			$scope.labels = [];
			if ($rootScope.accounts !== undefined) {
				angular.forEach($rootScope.accounts, function(account) {
					if (!account.creditCard) {
						$scope.data.push(account.currentBalance.number.toFixed(2));
						$scope.labels.push(account.name + "@" + $translate.instant(account.financialInstitution.name));
					}
				});
			}
			if ($scope.data.length === 0) {
				$scope.data = [1];
				$scope.labels.push($translate.instant("dashboard.no_accounts"));
			}
		};
		$rootScope.$on("networth-changed", function() {
			$scope.recalculateNetWorth();
		});
		if ($scope.labels === undefined) {
			$scope.recalculateNetWorth();
		}
	}]);

})(angular);
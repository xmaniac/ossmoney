(function(angular) {

	var admin = angular.module("ossmoneyAdmin");

	admin.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"entities.add.title": "Añadir entidad financiera",
			"entity.name": "Nombre de la entidad",
			"entity.web": "URL web",
			"entity.create": "Crear entidad financiera",
			"entity.validation.name": "El nombre es obligatorio",
			"entity.create.success": "Entidad dad de alta con éxito",
			"entity.create.error": "Error al dar de alta la entidad financiera",
			"pm.add.title": "Añadir metal precioso",
			"pm.add.metal": "Metal precioso",
			"pm.GOLD": "Oro",
			"pm.SILVER": "Plata",
			"pm.PLATINUM": "Platino",
			"pm.PALLADIUM": "Paladio",
			"pm.add.shape": "Forma",
			"pm.COIN": "Moneda",
			"pm.BAR": "Lingote",
			"pm.NUGGET": "Pepita",
			"pm.MEDAL": "Medalla",
			"pm.JEWEL": "Joya",
			"pm.add.mint": "CECA / Origen",
			"pm.UNKNOWN_MINT": "Desconocido",
			"pm.CHINA_MINT": "China",
			"pm.PERTH_MINT": "Australia (Perth Mint)",
			"pm.ROYAL_CANADIAN_MINT": "Canada (Royal Mint)",
			"pm.SOUTH_AFRICA_MINT": "Sudáfrica",
			"pm.UK_ROYAL_MINT": "Reino unido",
			"pm.US_MINT": "EEUU",
			"pm.MEXICO": "Méjico",
			"pm.SOMALIA": "Somalia",
			"pm.add.name": "Nombre del producto",
			"pm.add.url": "URL Imagen",
			"pm.add.create": "Guardar producto",
			"pm.validation.name": "Por favor, indique un nombre para el producto",
			"pm.add.create.success": "Producto creado",
			"pm.add.create.error": "Error al crear el producto",
		});
		$translateProvider.translations('en', {
			"entities.add.title": "Add financial entity",
			"entity.name": "Name",
			"entity.web": "Web url",
			"entity.create": "Create bank",
			"entity.validation.name": "The name is required",
			"entity.create.success": "Financial entity created",
			"entity.create.error": "Error creating financial entity",
			"pm.add.title": "Add precious metal product",
			"pm.add.metal": "Precious metal",
			"pm.GOLD": "Gold",
			"pm.SILVER": "Silver",
			"pm.PLATINUM": "Platinum",
			"pm.PALLADIUM": "Palladium",
			"pm.add.shape": "Shape",
			"pm.COIN": "Coin",
			"pm.BAR": "Ingot",
			"pm.NUGGET": "Nugget",
			"pm.MEDAL": "Medal",
			"pm.JEWEL": "Jewel",
			"pm.add.mint": "Mint / Origin",
			"pm.UNKNOWN_MINT": "Unkown mint",
			"pm.CHINA_MINT": "China",
			"pm.PERTH_MINT": "Australia (Perth Mint)",
			"pm.ROYAL_CANADIAN_MINT": "Canada (Royal Mint)",
			"pm.SOUTH_AFRICA_MINT": "South Africa Mint",
			"pm.UK_ROYAL_MINT": "UK Royal Mint",
			"pm.US_MINT": "US Mint",
			"pm.MEXICO": "Mexico",
			"pm.SOMALIA": "Somalia",
			"pm.add.name": "Product name",
			"pm.add.url": "Image URL",
			"pm.add.create": "Save product",
			"pm.validation.name": "Please, provide a name for the product",
			"pm.add.create.success": "Product created",
			"pm.add.create.error": "Error creating product",
		});
	}]);

})(angular);
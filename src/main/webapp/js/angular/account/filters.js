(function(angular) {

    var account = angular.module("ossmoneyAccount");

    account.filter('isEmpty', function () {
        return function (obj) {
            var empty = true;
            for (var bar in obj) {
                if (obj.hasOwnProperty(bar)) {
                    empty = false;
                }
            }
            return empty;
        };
    });

})(angular);
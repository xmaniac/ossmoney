(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"country": "País",
			"country.spain": "España",
			"country.en-us": "EEUU",
			"bank.unknown": "Desconocido/a",
			"bank": "Institución financiera",
			"menu_accounts_list": "Listado de cuentas",
			"menu_accounts_add": "Añadir nueva cuenta",
			"menu_deposit_add": "Contratar depósito",
			"menu_preferences": "Preferencias",
			"menu_administration": "Administración",
			"menu_entities": "Entidades financieras",
			"menu_metals": "Metales preciosos",
			"menu_pm_prices": "Precios de mercado",
			"account.name": "Nombre",
			"account.create": "Crear cuenta",
			"account.favorite": "Cuenta favorita",
			"account.type": "Tipo de cuenta",
			"account.balance.cc": "Disponible",
			"account.list.cc": "Tarjetas de crédito",
			"account.list.cc.dd": "No se han definido cuentas bancarias todavía",
			"account.list.cc.empty": "No se han definido cuentas de crédito todavía",
			"SAVING": "Cuenta de ahorro",
			"CREDIT_CARD": "Tarjeta de crédito",
			"DAY_TO_DAY": "Cuenta del día a día",
			"INVESTMENT": "Cuenta de inversiones",
			"account.iban": "Número de cuenta",
			"account.balance": "Balance inicial",
			"account.balance.or.credit": "Balance/crédito inicial",
			"account.balance.current": "Balance",
			"account.period": "Período",
			"account.opened": "Fecha de apertura",
			"account.favorite": "Incluir en favoritos",
			"account.income": "Ingresos",
			"account.expenses": "Gastos",
			"account.top": "Ranking",
			"account.by.amount": "Por importe",
			"account.by.category": "Por categoría",
			"account.add.title": "Añadir cuenta bancaria",
			"account.add.success": "La cuenta ha sido creada con éxito",
			"account.modify.success": "La cuenta ha sido modificada con éxito",
			"account.add.error": "Error al crear la cuenta",
			"account.modify.error": "Error al modificar la cuenta",
			"account.modify": "Modificar cuenta",
			"account.closed": "Cuenta cerrada",
			"account.modify.title": "Modificar cuenta",
			"account.description": "Descripción de la cuenta",
			"account.validation.name": "Por favor, provea de un nombre a la cuenta",
			"account.validation.balance": "El balance debe ser mayor que 0",
			"account.validation.bank": "Debe informarse la entidad financiera",
			"account.list.day_to_day": "Cuentas bancarias",
			"account.list.chart": "Cuentas",
			"account.list.deposits": "Depósitos",
			"deposit.duration.months": "meses",
			"account.details.since": "Ver los movimientos de",
			"account.transaction.nodesc": "Sin descripción",
			"account.transaction.empty": "No hay movimientos",
			"account.reopen": "Reabrir cuenta",
			"account.reopen.success": "Cuenta abierta de nuevo",
			"account.reopen.error": "Error al intentar reabrir la cuenta",
			"account.closed.title": "Esta cuenta se encuentra actualmente cerrada",
			"credit.card.reconciliation": "Cierre de extracto de tarjeta",
			"INCOME": "Ingreso",
			"EXPENSE": "Gasto",
			"TRANSFER": "Transferencia",
			"transaction.type": "Tipo de movimiento",
			"transaction.add.title": "Añadir movimiento",
			"transaction.modify.title": "Modificar operación",
			"transaction.memo": "Concepto",
			"transaction.referenceNumber": "Referencia",
			"transaction.operationDate": "Fecha de la operación",
			"transaction.operationDate.required": "Es necesario indicar la fecha del movimiento",
			"transaction.amount": "Importe",
			"transaction.amount.required": "El importe debe ser positivo para ingresos y negativo para gastos",
			"transaction.tags": "Etiquetas",
			"transaction.tags.add": "Añadir etiqueta",
			"transaction.create": "Añadir movimiento",
			"transaction.modify": "Guardar cambios",
			"transaction.search": "Buscar movimientos...",
			"transaction.delete.title": "Eliminar operación",
			"transaction.delete.type": "Escriba {{ amount }} para continuar",
			"transaction.delete.success": "Movimiento eliminado",
			"transaction.delete.error": "Error al eliminar el movimiento",
			"datepicker.close": "Cerrar",
			"transfer.direction": "Propósito de la transferencia",
			"transaction.chargeAccount": "Cuenta de destino",
			"transfer.direction.in": "Recepción de dinero",
			"transfer.direction.out": "Enviar dinero desde esta cuenta",
			"transaction.chargeAccount.no.account": "Cuenta externa",
			"transaction.add.success": "Operación almacenada",
			"transaction.add.error": "Error en el alta de movimiento",
			"accounts.export": "Exportar información",
			"account.pm.transfer": "Transferencia compra de metales preciosos",
			"account.pm.transfer.sell": "Transferencia venta de metales preciosos",
			"account.deposit.transfer": "Transferencia contratación de depósito",
			"account.delete.title": "Borrar cuenta",
			"account.close.title": "Cerrar cuenta",
			"account.delete.type": "ELIMINAR",
			"account.delete.type.message": "Escriba ELIMINAR para continuar",
			"account.delete.warning": "Se perderán todos los datos almacenados en el sistema para esta cuenta",
			"account.close.type": "CERRAR",
			"account.close.type.message": "Escriba CERRAR para continuar",
			"account.delete.ok": "Eliminar la cuenta",
			"account.close.ok": "Cerrar la cuenta",
			"account.close.success": "Cuenta cerrada con éxito",
			"account.delete.success": "Cuenta eliminada con éxito",
			"account.delete.error": "Error al procesar la petición",
			"transaction.reconcile.amount": "Importe del extracto",
			"transaction.reconcile.account": "Cargar cuenta",
			"transaction.reconcile.no.account": "Sin cuenta de cargo",
			"transaction.reconcile": "Cerrar extracto",
			"transaction.reconcile.account.tooltip": "Opcionalmente puede indicar una cuenta sobre la que realizar un cargo de importe equivalente al extracto cerrado",
			"transactions.reconcile.success": "Extracto cerrado",
			"transactions.reconcile.error": "Error cerrando el extracto"
		});
		$translateProvider.translations('en', {
			"country": "Country",
			"country.spain": "Spain",
			"country.en-us": "USA",
			"bank.unknown": "Unknown",
			"bank": "Financial institution",
			"menu_accounts_list": "Accounts",
			"menu_accounts_add": "Add new account",
			"menu_deposit_add": "Contract deposit",
			"menu_preferences": "Preferences",
			"menu_administration": "Administration",
			"menu_entities": "Financial entities",
			"menu_metals": "Precious metals",
			"menu_pm_prices": "Spot prices",
			"account.name": "Name",
			"account.iban": "Account number",
			"account.favorite": "Favorite account",
			"account.type": "Type of account",
			"account.balance.cc": "Available",
			"account.list.cc": "Credit card accounts",
			"account.list.cc.dd": "No bank accounts defined",
			"account.list.cc.empty": "No credit card accounts defined",
			"SAVING": "Savings account",
			"CREDIT_CARD": "Credit Card",
			"DAY_TO_DAY": "Day to day account",
			"INVESTMENT": "Investment account",
			"account.create": "Create account",
			"account.balance": "Initial balance",
			"account.balance.or.credit": "Initial balance/credit",
			"account.balance.current": "Balance",
			"account.period": "Period",
			"account.opened": "Open date",
			"account.favorite": "Add to favorites",
			"account.income": "Income",
			"account.expenses": "Expenses",
			"account.top": "Ranking",
			"account.by.amount": "Amount",
			"account.by.category": "Category",
			"account.add.title": "Add new bank account",
			"account.add.success": "Account created",
			"account.modify.success": "Account modified",
			"account.add.error": "Error creating account",
			"account.modify.error": "Error modifying account",
			"account.modify": "Modify account",
			"account.closed": "Account closed",
			"account.modify.title": "Modify account",
			"account.description": "Account description",
			"account.validation.name": "Please, provide a name for the account",
			"account.validation.balance": "Please introduce a number greater than 0",
			"account.validation.bank": "Please select a financial institution",
			"account.list.day_to_day": "Bank accounts",
			"account.list.chart": "Accounts",
			"account.list.deposits": "Certificate of deposit",
			"deposit.duration.months": "months",
			"account.details.since": "See movements from ",
			"account.transaction.nodesc": "N/A",
			"account.transaction.empty": "No movements",
			"account.reopen": "Reopen account",
			"account.reopen.error": "Error reopening account",
			"account.reopen.success": "Account reopened",
			"account.closed.title": "This account is currently closed",
			"credit.card.reconciliation": "Credit card reconciliation",
			"INCOME": "Income",
			"EXPENSE": "Expenses",
			"TRANSFER": "Transfer",
			"transaction.type": "Movement type",
			"transaction.add.title": "Add movement",
			"transaction.modify.title": "Modify operation",
			"transaction.memo": "Description",
			"transaction.referenceNumber": "Reference",
			"transaction.operationDate": "Operation date",
			"transaction.operationDate.required": "Please supply a date for this movement",
			"transaction.amount": "Amount",
			"transaction.amount.required": "The amount must be positive for income and negative for expenses",
			"transaction.tags": "Tags",
			"transaction.tags.add": "Add a tag",
			"transaction.create": "Add movement",
			"transaction.modify": "Save modifications",
			"transaction.search": "Search movements...",
			"transaction.delete.title": "Delete movement",
			"transaction.delete.type": "Type {{ amount }} to continue",
			"transaction.delete.success": "Movement removed",
			"transaction.delete.error": "Error removing movement",
			"datepicker.close": "Close",
			"transaction.chargeAccount": "Destination account",
			"transfer.direction": "Purpose of the transfer",
			"transfer.direction.in": "Receive funds",
			"transfer.direction.out": "Send money from this account",
			"transaction.chargeAccount.no.account": "External account",
			"transaction.add.success": "Movement saved",
			"transaction.add.error": "Error saving movement",
			"accounts.export": "Export accounts",
			"account.pm.transfer": "Purchase of precious metals",
			"account.pm.transfer.sell": "Sell of precious metals",
			"account.deposit.transfer": "Deposit signup transfer",
			"account.delete.title": "Delete account",
			"account.close.title": "Close account",
			"account.delete.type": "REMOVE",
			"account.delete.type.message": "Type REMOVE to continue",
			"account.delete.warning": "All data for this account will be erased",
			"account.close.type": "CLOSE",
			"account.close.type.message": "Type CLOSE to continue",
			"account.delete.ok": "Remove account",
			"account.close.ok": "Close account",
			"account.close.success": "Account successfully closed",
			"account.delete.success": "Account successfully deleted",
			"account.delete.error": "Unexpected server error",
			"transaction.reconcile.amount": "Total credit amount",
			"transaction.reconcile.account": "Charge account",
			"transaction.reconcile.no.account": "No charge account",
			"transaction.reconcile": "Reconcile movements",
			"transaction.reconcile.account.tooltip": "You can optionally provide an account. It will be charged the same amount as the total amount of the reconciled movements",
			"transactions.reconcile.success": "Movements reconciled",
			"transactions.reconcile.error": "Error reconciling movements"
		});
	}]);

})(angular);

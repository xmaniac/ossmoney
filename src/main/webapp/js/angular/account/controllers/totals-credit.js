(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountTotalsCreditController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.recalculateTotal = function() {
			$scope.totalCredit = 0;
			angular.forEach($rootScope.accounts, function(account) {
				if (account.creditCard) {
					$scope.totalCredit -= Math.abs(account.currentBalance.number - account.initialBalance.number);
				}
			});
		};
		$rootScope.$on("accounts-changed", function() {
			$scope.recalculateTotal();
		});
		if ($scope.totalCredit === undefined) {
			$scope.recalculateTotal();
		}
	}]);

})(angular);
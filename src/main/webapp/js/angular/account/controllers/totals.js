(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountTotalsController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.recalculateTotal = function() {
			var accounts = $rootScope.accounts || [];
			$scope.total = accounts.reduce(function(previousValue, account) {
				return previousValue + (account.creditCard ? 0 : account.currentBalance.number);
			}, 0);
		};
		$rootScope.$on("accounts-changed", function() {
			$scope.recalculateTotal();
		});
		if ($scope.total === undefined) {
			$scope.recalculateTotal();
		}
	}]);

})(angular);
(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountInitController", ["$scope", "$state", function($scope, $state) {
		$state.go("account_main");
	}]);

})(angular);
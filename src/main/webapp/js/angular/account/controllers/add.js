(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountAddController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "$locale", "FinancialInstitution", "Account", "Alert", "Array", "ISO", function($rootScope, $scope, $state, $stateParams, $translate, $locale, FinancialInstitution, Account, Alert, Array, ISO) {
		$scope.loading = false;
		$scope.nameValidation = "";
		$scope.bankValidation = ""
		$scope.balanceValidation = "";
		$scope.country = "country." + $rootScope.locale;
		$scope.account = $stateParams.account || {
			"favorite": false,
			"accountType": $rootScope.accountTypes !== undefined ? $rootScope.accountTypes[0] : null,
			"initialBalance": {"currency": $rootScope.currency || {"currencyCode": "EUR"}, "number": 0}
		};
		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.saveAccount = function() {
			$scope.nameValidation = !$scope.account.name ? "has-error has-feedback" : "";
			$scope.bankValidation = !$scope.account.financialInstitution ? "has-error has-feedback" : "";
			$scope.balanceValidation = (($scope.account.initialBalance.number === null) || ($scope.account.initialBalance.number < 0)) ? "has-error has-feedback" : "";
			if (!$scope.nameValidation & !$scope.balanceValidation & !$scope.bankValidation) {
				$scope.loading = true;
				var account = Account.save({
					"uuid": $scope.account.uuid,
					"name": $scope.account.name,
					"iban": $scope.account.iban,
					"favorite": $scope.account.favorite,
					"description": $scope.account.description,
					"accountType": $scope.account.accountType,
					"opened": ISO.asDate($scope.account.opened),
					"initialBalance": Number($scope.account.initialBalance.number),
					"financialInstitution": $scope.account.financialInstitution.uuid
				}, function() {
					$scope.loading = false;
					if (account && account.currentBalance) {
						Array.remove($rootScope.accounts, $scope.account.uuid);
						$rootScope.accounts.push(account);
						$rootScope.$broadcast("accounts-changed");
						$rootScope.transactions[account.uuid] = [];
						$state.go("account_main");
						Alert.show($translate.instant($scope.account.uuid !== undefined ? "account.modify.success" : "account.add.success"));
					} else {
						Alert.warn($translate.instant($scope.account.uuid !== undefined ? "account.modify.error" : "account.add.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant($scope.account.uuid !== undefined ? "account.modify.error" : "account.add.error"));
				}); 
			}
		};
		$scope.substituteFinancialInstitution = function() {
			angular.forEach($rootScope.banks[$scope.country], function(bank) {
				if (bank.uuid === $scope.account.financialInstitution.uuid) {
					$scope.account.financialInstitution = bank;
				}
			});
		};
		$scope.findFinancialInstitutions = function(country, callback) {
			var banks = FinancialInstitution.query({
				"country": country
			}, function() {
				$rootScope.banks[country] = banks;
				callback(banks);
			});
		};
		if ($rootScope.banks[$scope.country] === undefined) {
			$scope.findFinancialInstitutions($scope.country, function(banks) {
				if ($scope.account.uuid === undefined) {
					$scope.account.financialInstitution = banks.length > 0 ? banks[0] : null;
				} else {
					$scope.substituteFinancialInstitution();
				}
			});
		} else if ($scope.account.uuid === undefined) {
			$scope.account.financialInstitution = $rootScope.banks[$scope.country].length > 0 ? $rootScope.banks[$scope.country][0] : null;
		} else {
			$scope.substituteFinancialInstitution();
		}
	}]);

})(angular);
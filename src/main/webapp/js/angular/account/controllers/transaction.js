(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyTransactionAddController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "Transaction", "Alert", "ISO", function($rootScope, $scope, $state, $stateParams, $translate, Transaction, Alert, ISO) {
		$scope.opened = false;
		$scope.loading = false;
		$scope.today = new Date();
		$scope.dateValidation = "";
		$scope.balanceValidation = "";
		$scope.account =  $stateParams.account;
		$scope.transaction = angular.copy($stateParams.transaction) || {
			"tags": [],
			"amount": {
				"number": 0,
				"currency": $scope.account.currentBalance.currency
			}, "operationDate": new Date(),
			"transactionType": "EXPENSE",
			"account": $scope.account.uuid 
		};
		var tags = [];
		angular.forEach($scope.transaction.tags, function(tag) {
			tags.push({
				"tag": tag.tag,
				"text": tag.text
			});
		});
		$scope.transaction.tags = tags;
		$scope.transaction.chargeAccount = "";
		$scope.originalAmount = $scope.transaction.amount.number;
		$scope.direction = $scope.transaction.amount.number <= 0 ? "out" : "in";
		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.loadItems = function(query) {
			var found = [];
			angular.forEach($rootScope.tags, function(value) {
				var show = $translate.instant(value.tag);
				if (show.toLowerCase().startsWith(query.toLowerCase())) {
					found.push({
						"text": show,
						"tag": value.tag
					});
				}
			});
			return found;
		};
		$scope.byAccountName = function(a) {
			console.log(a);
			return function(chargeAccount) {
				console.log(chargeAccount);
				return chargeAccount.name === $scope.account.name;
			};
		};
		$scope.createTransaction = function() {
			$scope.dateValidation = !$scope.transaction.operationDate? "has-error has-feedback" : "";
			$scope.balanceValidation = !$scope.transaction.amount.number ? "has-error has-feedback" :
				$scope.transaction.amount.number === 0 ? "has-error has-feedback" :
					$scope.transaction.amount.number > 0 & $scope.transaction.transactionType === "EXPENSE" ? "has-error has-feedback" : 
						$scope.transaction.amount.number < 0 & $scope.transaction.transactionType === "INCOME" ? "has-error has-feedback" :
							$scope.transaction.amount.number > 0 & $scope.transaction.transactionType === "TRANSFER" & $scope.direction === "out" ? "has-error has-feedback" :
								$scope.transaction.amount.number < 0 & $scope.transaction.transactionType === "TRANSFER" & $scope.direction === "in" ? "has-error has-feedback" : "";
			
			if (!$scope.dateValidation & !$scope.balanceValidation) {
				$scope.loading = true;
				var dto = angular.copy($scope.transaction);
				dto.tags = [];
				dto.amount = Number(dto.amount.number);
				dto.operationDate = ISO.asDate(dto.operationDate);
				dto.chargeAccount = dto.chargeAccount ? dto.chargeAccount.uuid : null;
				angular.forEach($scope.transaction.tags, function(tag) {
					dto.tags.push(tag.tag);
				});
				var transaction = Transaction.save(dto, function() {
					$scope.loading = false;
					if (transaction && transaction.amount) {
						angular.forEach($rootScope.accounts, function (account) {
							if (account.uuid === $scope.account.uuid) {
								account.currentBalance.number += $scope.transaction.amount.number - $scope.originalAmount;
								$scope.account = account; 
							} else if (account.uuid === dto.chargeAccount) {
								account.currentBalance.number -= $scope.transaction.amount.number;
							}
						});
						$rootScope.$broadcast("accounts-changed");
						delete $rootScope.incomeVsExpenses;
						$rootScope.$broadcast("interval-changed", {
							"months": $rootScope.incomeVsExpensesFromDate
						});
						delete $rootScope.budgets;
						$rootScope.transactions[dto.uuid] = [];
						$rootScope.transactions[$scope.account.uuid] = [];
						$state.go("account_details", {
							"account": $scope.account
						});
						Alert.show($translate.instant("transaction.add.success"));
					} else {
						Alert.warn($translate.instant("transaction.add.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("transaction.add.error"));
				}); 
			}
		};
	}]);

})(angular);
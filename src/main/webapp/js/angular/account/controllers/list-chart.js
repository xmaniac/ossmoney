(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountDepositChartController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.loadingChart = $rootScope.accounts === undefined | $rootScope.portfolio === undefined;
		$scope.recalculatePie = function() {
			if (!$scope.loadingChart) {
				var totalAccount = $rootScope.accounts.reduce(function(previousValue, account) {
					return previousValue + (account.creditCard ? account.currentBalance.number - account.initialBalance.number : account.currentBalance.number);
				}, 0);
				var totalDeposit = $rootScope.portfolio.deposits.reduce(function(previousValue, deposit) {
					return previousValue + deposit.balance.number;
				}, 0);
				$scope.data = [totalAccount, totalDeposit];
				$scope.labels = [$translate.instant("account.list.chart"), $translate.instant("account.list.deposits")];
			}
		};
		$rootScope.$on("accounts-changed", function() {
			$scope.loadingChart = $rootScope.accounts === undefined | $rootScope.portfolio === undefined;
			$scope.recalculatePie();
		});
		$rootScope.$on("portfolio-changed", function() {
			$scope.loadingChart = $rootScope.accounts === undefined | $rootScope.portfolio === undefined;
			$scope.recalculatePie();
		});
		$scope.recalculatePie();
	}]);

})(angular);
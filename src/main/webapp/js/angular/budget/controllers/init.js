(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetInitController", ["$rootScope", "$state", "$translate", "Budget", function($rootScope, $state, $translate, Budget) {
		if ($rootScope.budgets === undefined) {
			$rootScope.budgets = [];
			$rootScope.budgetTransactions = [];
			var budgets = Budget.query(function() {
				if (budgets && budgets.length > 0) {
					$rootScope.budgets = budgets;
					angular.forEach(budgets, function(budget) {
						angular.forEach(budget.transactions, function(transaction) {
							$rootScope.budgetTransactions.push(transaction);
							angular.forEach(transaction.tags, function(tag) {
								tag.text = $translate.instant(tag.tag);
							});
						});
					});
					$rootScope.$broadcast("budget-changed");
				} else {
					$state.go("add_budget");
				}
			});
		}
	}]);

})(angular);
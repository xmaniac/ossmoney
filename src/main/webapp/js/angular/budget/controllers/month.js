(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetMonthController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		if ($rootScope.skin === "brown") {
			$scope.colours_budget = [
				{fillColor: "rgba(194,100,72,0.2)", strokeColor: "rgba(194,100,72,1)", pointColor: "rgba(194,100,72,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(194,100,72,0.8)"},
				{fillColor: "rgba(217,178,113,0.2)", strokeColor: "rgba(217,178,113,1)", pointColor: "rgba(217,178,113,1)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(217,178,113,0.8)"}
			];
		}
		$scope.month = $translate.instant("month." + new Date().getMonth()); 
		$scope.recalculateMonthlyBudget = function() {
			$scope.labels_budget = [];
			$scope.data_budget = [[], []];
			$scope.series_budget = [$translate.instant("budget.actual"), $translate.instant("budget.estimation")];
			angular.forEach($rootScope.budgets, function(budget) {
				$scope.data_budget[0].push(budget.spent);
				$scope.data_budget[1].push(budget.amount.number);
				$scope.labels_budget.push($translate.instant(budget.subcategory.tag));
			});
		};
		$rootScope.$on("budget-changed", function() {
			$scope.recalculateMonthlyBudget();
		});
		$scope.recalculateMonthlyBudget();
	}]);

})(angular);
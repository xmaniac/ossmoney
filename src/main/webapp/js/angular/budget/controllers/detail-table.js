(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetDetailTableController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "Budget", "Array", "Alert", function($rootScope, $scope, $state, $stateParams, $translate, Budget, Array, Alert) {
		$scope.transactions = $rootScope.budgets[$stateParams.budget - 1].transactions;
	}]);

})(angular);
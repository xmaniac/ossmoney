(function(angular) {

	var budget = angular.module("ossmoneyBudget");

	budget.controller("OssmoneyBudgetAddController", ["$rootScope", "$scope", "$state", "$translate", "Alert", "Budget", function($rootScope, $scope, $state, $translate, Alert, Budget) {
		var today = new Date();
		$scope.openedTo = false;
		$scope.openedFrom = false;
		var monthStart = new Date(today.getFullYear(), today.getMonth(), 1);
		var monthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);

		$scope.loading = false;
		$scope.toValidation = "";
		$scope.tagValidation = "";
		$scope.fromValidation = "";
		$scope.nameValidation = "";
		$scope.balanceValidation = "";
		$scope.selectedCategory = 
		$scope.budget =  {
			"amount": 0,
			"to": monthEnd,
			"from": monthStart,
			"name": "",
			"subcategory": null
		};

		var categories = {};
		$scope.categories = [];
		angular.forEach($rootScope.tags, function(tag) {
			angular.forEach(tag.categories, function(category) {
				if (!categories[category.tag]) {
					categories[category.tag] = {
						"tag": category.tag,
						"subcategories": [],
						"text": $translate.instant(category.tag)
					};
					$scope.categories.push(categories[category.tag]);
				}
				categories[category.tag].subcategories.push({
					"tag": tag.tag,
					"text": $translate.instant(tag.tag)
				});
			});
		});
		$scope.selectedCategory = $scope.categories[0];

		$scope.openTo = function($event) {
			$scope.openedTo = true;
			$scope.openedFrom = false;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.openFrom = function($event) {
			$scope.openedTo = false;
			$scope.openedFrom = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		
		$scope.saveBudget = function() {
			$scope.toValidation = !$scope.budget.to ? "has-error" : "";
			$scope.fromValidation = !$scope.budget.from ? "has-error" : "";
			$scope.nameValidation = !$scope.budget.name ? "has-error" : "";
			$scope.balanceValidation = !$scope.budget.amount ? "has-error" : "";
			$scope.tagValidation = !$scope.budget.subcategory ? "has-error" : "";
			if (!$scope.nameValidation & !$scope.fromValidation && !$scope.toValidation & !$scope.balanceValidation && !$scope.tagValidation) {
				$scope.loading = true;
				var dto = angular.copy($scope.budget);
				dto.subcategory = dto.subcategory.tag; 
				var budget = Budget.save(dto, function() {
					$scope.loading = false;
					if (budget && budget.uuid) {
						$rootScope.budgets.push(budget);
						$rootScope.$broadcast("budget-changed");
						$state.go("budget");
						Alert.show($translate.instant("budget.save.success"));
					} else {
						Alert.warn($translate.instant("budget.save.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("budget.save.error"));
				}); 
			}
		};
	}]);

})(angular);
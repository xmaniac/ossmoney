(function(window, angular) {

	var account = angular.module("ossmoneyBudget");

	account.factory("Budget", ['$resource', function($resource) {
		return $resource("budget/:uuid", {}, {
			"query": {
				isArray: true
			}, "save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}, "remove": {
				"method": "DELETE",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

})(window, angular);
(function(angular) {

	angular.module("ossmoneyBudget", ["smart-table", "ui.router", "pascalprecht.translate"]);

})(angular);
(function(window, angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.factory("PreciousMetalPrices", ['$resource', function($resource) {
		return $resource("/prices", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	investment.factory("Investment", ['$resource', function($resource) {
		return $resource("investment", {}, {
			"query": {
				"method": "GET"
			}
		});
	}]);

	investment.factory("Deposit", ['$resource', function($resource) {
		return $resource("investment/deposit", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	investment.factory("DepositCancellation", ['$resource', function($resource) {
		return $resource("investment/deposit/cancel", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	investment.factory("PreciousMetalProduct", ['$resource', function($resource) {
		return $resource("investment/pm/products", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	investment.factory("PreciousMetalTransaction", ['$resource', function($resource) {
		return $resource("investment/pm", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	investment.factory("PreciousMetalSellTransaction", ['$resource', function($resource) {
		return $resource("investment/pm/sell", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

})(window, angular);
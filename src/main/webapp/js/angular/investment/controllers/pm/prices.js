(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyInvestmentPreciousMetalPricesController", ["$rootScope", "PreciousMetalPrices", function($rootScope, PreciousMetalPrices) {
		if ($rootScope.preciousMetalPrices === undefined) {
			var prices = PreciousMetalPrices.query({}, function() {
				$rootScope.preciousMetalPrices = prices;
			});
		}
	}]);

})(angular);
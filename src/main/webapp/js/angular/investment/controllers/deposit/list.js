(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositListController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.loading = $rootScope.portfolio === undefined;
		$scope.deposits = $rootScope.portfolio !== undefined ? $rootScope.portfolio.deposits : [];
		$rootScope.$on("portfolio-changed", function() {
			$scope.loading = false;
			$scope.deposits = $rootScope.portfolio.deposits;
		});
	}]);

})(angular);
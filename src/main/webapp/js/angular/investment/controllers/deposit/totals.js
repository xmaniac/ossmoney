(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositTotalsController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.totalDeposit = 0;
		$scope.recalculateTotals = function() {
			if ($rootScope.portfolio !== undefined) {
				$scope.totalDeposit = $rootScope.portfolio.deposits.reduce(function(previousValue, deposit) {
					return previousValue + deposit.balance.number;
				}, 0);
			}
		};
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculateTotals();
		});
		$scope.recalculateTotals();
	}]);

})(angular);
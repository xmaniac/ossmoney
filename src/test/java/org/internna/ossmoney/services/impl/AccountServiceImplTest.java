package org.internna.ossmoney.services.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.Date;
import java.util.Optional;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.io.ByteArrayInputStream;
import org.internna.ossmoney.model.Country;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.model.account.AccountType;
import org.internna.ossmoney.model.account.FinancialInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class AccountServiceImplTest extends AbstractSecuredTest {

    private @Autowired AccountService accountService;

    @Test(expected = IllegalArgumentException.class)
    public void testFind() {
        assertThat(accountService.find(null).isPresent(), is(false));
        assertThat(accountService.find("8483").isPresent(), is(false));
        assertThat(accountService.find("1").isPresent(), is(true));
        loginAsTestUser();
        accountService.find("1");
    }

    @Test
    public void testFindAccounts() {
        assertThat(accountService.findAccounts(), hasSize(4));
        loginAsTestUser();
        assertThat(accountService.findAccounts(), hasSize(0));
    }

    @Test
    public void testFindFinancialInstitutions() {
        assertThat(accountService.findFinancialInstitutions("country.es_ES"), hasSize(3));
        assertThat(accountService.findFinancialInstitutions("country.whatever"), hasSize(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testClose() {
        assertThat(accountService.close(null).isPresent(), is(false));
        assertThat(accountService.close("343241").isPresent(), is(false));
        Account closed = accountService.close("1").get();
        assertThat(closed.isVisible(), is(true));
        assertThat(closed.getClosed(), is(notNullValue()));
        assertThat(accountService.close("1").isPresent(), is(false));
        assertThat(accountService.find("1").isPresent(), is(false));
        loginAsTestUser();
        accountService.close("2");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReopen() {
        assertThat(accountService.reopen(null).isPresent(), is(false));
        assertThat(accountService.reopen("1344").isPresent(), is(false));
        assertThat(accountService.close("1").get().getClosed(), is(notNullValue()));
        assertThat(accountService.reopen("1").get().getClosed(), is(nullValue()));
        accountService.close("1");
        loginAsTestUser();
        accountService.reopen("1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSave() {
        Account dto = new Account();
        dto.setName("New account");
        dto.setAccountType(AccountType.DAY_TO_DAY);
        dto.setFinancialInstitution(new FinancialInstitution("1"));
        Optional<Account> account = accountService.save(dto, BigDecimal.ONE);
        assertThat(account.isPresent(), is(true));
        assertThat(account.get().isVisible(), is(true));
        assertThat(account.get().getName(), is(equalTo("New account")));
        assertThat(account.get().getInitialBalance(), is(equalTo(account.get().getCurrentBalance())));
        assertThat(account.get().getInitialBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.ONE)));
        dto.setName("Renamed");
        dto.setUuid(account.get().getUuid());
        account = accountService.save(dto, BigDecimal.TEN);
        assertThat(account.isPresent(), is(true));
        assertThat(account.get().isVisible(), is(true));
        assertThat(account.get().getName(), is(equalTo("Renamed")));
        assertThat(account.get().getInitialBalance(), is(equalTo(account.get().getCurrentBalance())));
        assertThat(account.get().getInitialBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(BigDecimal.TEN)));
        dto = new Account();
        dto.setUuid("5");
        dto.setFavorite(false);
        dto.setOpened(new Date());
        dto.setName("Old account");
        dto.setAccountType(AccountType.DAY_TO_DAY);
        dto.setFinancialInstitution(new FinancialInstitution("1"));
        final BigDecimal thousand = BigDecimal.TEN.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
        account = accountService.save(dto, thousand);
        assertThat(account.get().getInitialBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(thousand)));
        assertThat(account.get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal(650))));
        loginAsTestUser();
        accountService.save(dto, BigDecimal.ONE);
    }

    @Test
    public void testSaveFinancialInstitution() {
        final FinancialInstitution bank = new FinancialInstitution();
        bank.setCountry(new Country());
        bank.getCountry().setCountryKey("country.es_ES");
        assertThat(accountService.save(bank).isPresent(), is(false));
        bank.setName("A bank");
        final Optional<FinancialInstitution> created = accountService.save(bank);
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getName(), is(equalTo("A bank")));
        assertThat(accountService.save((FinancialInstitution) null).isPresent(), is(false));
    }

    @Test
    public void testExport() throws IOException {
        final byte[] data = accountService.export();
        final ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(data));
        int entries = 1;
        ZipEntry entry = zip.getNextEntry();
        assertThat(entry, is(notNullValue()));
        while ((entry = zip.getNextEntry()) != null) {
            entries++;
        }
        assertThat(entries, is(equalTo(4)));
    }

}